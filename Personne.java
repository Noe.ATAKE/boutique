/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;//package d'appartenance

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
/**
 *
 * @author NSQUARE
 */
public class Personne implements Serializable{
    //attributs
    protected long id;
    protected String nom;
    protected String prenoms;
    protected LocalDate dateNaissance;
    
    //constructeur
    public Personne(){
    
    }
    
    // setters
    public void setId(long id){
    
        this.id=id;
    }
    public void setNom(String nom){
    
        this.nom=nom;
    }
    public void setPrenoms(String pr){
    
        this.prenoms=pr;
    }
    public void setDateNaissance(LocalDate naiss){
    
        this.dateNaissance=naiss;
    }
    
    // getters
    public long getId(){
    
        return id;
    }
    public String getNom(){
    
        return nom;
    }
    public String getPrenoms(){
    
        return prenoms;
    }
    public LocalDate getDateNaissance(){
    
        return dateNaissance;
    }
    
    //differentes methodes
    public int getAge(){
        LocalDate today= LocalDate.now();
        return today.getYear() - getDateNaissance().getYear();
    }
    
    public int getAge(LocalDate dateReference){
        return LocalDate.now().getYear() - dateReference.getYear();
    }

    @Override //redefinition de la methode toString()
    public String toString() {
        return "Personne{" + "id=" + id + ", nom=" + nom + ", prenoms=" + prenoms + ", dateNaissance=" + dateNaissance + '}';
    }

    @Override //redefinition de la methode hashcode()
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 59 * hash + Objects.hashCode(this.nom);
        hash = 59 * hash + Objects.hashCode(this.prenoms);
        hash = 59 * hash + Objects.hashCode(this.dateNaissance);
        return hash;
    }

    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Personne other = (Personne) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
}
