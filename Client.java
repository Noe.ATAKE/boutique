/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites; //package d'appartenance

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
class Client extends Personne implements Serializable{
    private ArrayList <Achat> achat; //liste des achats opperes par le client
    private ArrayList <Produit> produit; //liste des produits
    
    // les attributs
   private String cin;
   private String carteVisa;

   // le constructeur
    public Client() {
        
    }

    //les setters
    public void setCin(String cin) {
        this.cin = cin;
    }
    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    //les getters
    public String getCin() {
        return cin;
    }
    public String getCarteVisa() {
        return carteVisa;
    }
    

    @Override //redefinition de la methode toString()
    public String toString() {
        return "Client{" + "achat=" + achat + ", produit=" + produit + ", cin=" + cin + ", carteVisa=" + carteVisa + '}';
    }

    @Override //redefinition de la methode hashCode(Object)
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.cin);
        hash = 97 * hash + Objects.hashCode(this.carteVisa);
        return hash;
    }

    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.cin, other.cin)) {
            return false;
        }
        return true;
    }
   
   
}
