/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites; //package d'appartenance

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
class ProduitAchete implements Serializable {
    private Produit produit; // produit identifie comme achete
    private Achat achat; // achat concerne par le produit achete
    
    //les attributs
    private static int quantite = 1; 
    private static double remise = 0.0;
    
    //constructeur
    public ProduitAchete() {
        
    }

    // les setters
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    //les getters
    public int getQuantite() {
        return quantite;
    }
    public double getRemise() {
        return remise;
    }
    
    //les differentes methodes
    public static double getPrixTotal()
    {
        return Produit.getPrixUnitaire()*quantite*(1-remise); 
    }
    
    @Override ////redefinition de la methode toString()
    public String toString() {
        return "ProduitAchete{" + "produit=" + produit + ", achat=" + achat + ", quantite=" + quantite + 
                ", remise=" + remise + '}';
    }

    @Override //redefinition de la methode hashCode()
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.produit);
        hash = 17 * hash + Objects.hashCode(this.achat);
        hash = 17 * hash + this.quantite;
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        return hash;
    }

    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        return true;
    }
    
}
