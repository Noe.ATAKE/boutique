/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;//package d'appartenance 

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
public class Categorie implements Serializable{
    private ArrayList <Produit> produit; // liste des produit concernes par la categorie
   
    //les attributs
    private int id ; 
    private String libelle;
    private String description;
    
    //constructeur
    public Categorie(){

    }
    
    //les setters
    public void setId(int id){
    
        this.id=id;
    }
    public void setLibelle(String lib){
    
        this.libelle=lib;
    }
    public void setDescription(String desc){
    
        this.description=desc;
    }
    
    //les getters
    public int getId(){
    
        return id;
    }
    public String getLibelle(){
    
        return libelle;
    }
    public String getDescription(){
    
        return description;
    }
    
    @Override //redefinition de la methode toString()
    public String toString(){
    
        return "Categorie{" + "numero=" + id + ", libelle=" + libelle + ", description=" + description + "}";
     }

    @Override //redefinition de la methode hashCode()
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        hash = 37 * hash + Objects.hashCode(this.libelle);
        hash = 37 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categorie other = (Categorie) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
}
