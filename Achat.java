/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites; //package d'appartenance

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
class Achat implements Serializable {
    private ArrayList <ProduitAchete> produitAchete; //liste des produits concernes par l'achat
    private Client client; // client ayant effectuer l'achat
    private Employe employe; //client ayant servi le client 
    
    // les attributs
    private long id;
    private LocalDateTime dateAchat;
    private double remise = 0.0;
    
    // constructeur
    public Achat() {
        
    }
    
    //les setters
    public void setId(long id) {
        this.id = id;
    }
    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }
    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    //les getters
    public long getId() {
        return id;
    }
    public LocalDateTime getDateAchat() {
        return dateAchat;
    }
    public double getRemise() {
        return remise;
    }

    // les methodes
    public double getPrixTotal(){
        int i;
        double somme=0;
        for(i=0; i<produitAchete.size(); i++){
            somme+= ProduitAchete.getPrixTotal();
        }
        return somme*(1-remise);
    }
    
    @Override //redefinition de la methode toString()
    public String toString() {
        return "Achat{" + "produitAchete=" + produitAchete + ", client=" + 
                client + ", employe=" + employe + ", id=" + id + ", dateAchat=" + 
                dateAchat + ", remise=" + remise + '}';
    }

    @Override //redefinition de la methode hashCode()
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 53 * hash + Objects.hashCode(this.dateAchat);
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        return hash;
    }

   
    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
     }
}
