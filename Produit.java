/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites; //package d'appartenance

import java.io.Serializable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
class Produit implements Serializable {
    private ArrayList <ProduitAchete> produitAchete; //liste des produit achetes
    private Client client; // client ayant achete le produit
    private Categorie categorie; //categorie du produit
    
    // les attributs
    private long id;
    private String libelle;
    private static double prixUnitaire;
    private LocalDate datePeremption;
    
    //constructeur
    public Produit() {
    }
    
    // les setters
    public void setId(long id) {
        this.id = id;
    }
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }
    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }
    
    //les getters
    public long getId() {
        return id;
    }
    public String getLibelle() {
        return libelle;
    }
    public static double getPrixUnitaire() {
        return prixUnitaire;
    }
    public LocalDate getDatePeremption() {
        return datePeremption;
    }
    
    // les methodes
    public boolean estPerime(){
        if(ChronoUnit.DAYS.between(LocalDate.now(), getDatePeremption())<0 ){
            return true;
        }else{
            return false;
        }
    }
   
    public boolean estPerime(LocalDate dateReference){
        if(ChronoUnit.DAYS.between(dateReference, getDatePeremption())<0 ){
            return true;
        }else{
            return false;
        }
    }

    @Override //redefinition de la methode toString()
    public String toString() {
        return "Produit{" + "produitAchete=" + produitAchete + ", client=" + client + ", categorie=" + 
                categorie + ", id=" + id + ", libelle=" + libelle + ", prixUnitaire=" + prixUnitaire + 
                ", datePeremption=" + datePeremption + '}';
    }

    @Override //redefinition de la methode hashCode()
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.libelle);
        hash = 43 * hash + (int) (Double.doubleToLongBits(this.prixUnitaire) ^ (Double.doubleToLongBits(this.prixUnitaire) >>> 32));
        hash = 43 * hash + Objects.hashCode(this.datePeremption);
        return hash;
    }

    @Override //redefinition de la methode equals(Object)
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
}
