/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites; //package d'appartenance

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author NSQUARE
 */
class Employe extends Personne implements Serializable{
    private ArrayList <Achat> achat; //liste des achats effectues par un employe
    
    // attributs
    private String csnn;

    // constructeur
    public Employe() {
        
    }

    // les setters
    public void setCsnn(String csnn) {
        this.csnn = csnn;
    }
    public String getCsnn() {
        return csnn;
    }

    @Override //redefinition de la methode toString()
    public String toString() {
        return "Employe{" + "achat=" + achat + ", csnn=" + csnn + '}';
    }

    @Override  //redefinition de la methode hashCode()
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.csnn);
        return hash;
    }

    @Override //redefinition de la methode equals()
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        if (!Objects.equals(this.csnn, other.csnn)) {
            return false;
        }
        return true;
    }
    
}
